This script gather all images in FROM construction of Dockerfile in every provided repo.

Then Snyk scans them against vulnerabilities with level of critical and high.

Then script sends report in text and json format to Mattermost channel and to Minio storage.
