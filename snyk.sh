#!/bin/bash
# set -eEu -o pipefail # check bash strict mode
# set -x # for debug

## declare an array variable
declare -a images=( \
"docker-images.jobtechdev.se/batfish/jobtech-taxonomy-api:latest"
"docker-images.jobtechdev.se/batfish/jobtech-taxonomy-public:latest"
)

# list of repos one can get by dry run: 
# ghorg clone --dry-run --fetch-all --path=. --preserve-dir  --protocol=ssh --scm=gitlab --token=<gitlab_token> arbetsformedlingen|sed '/^[[:space:]]*$/d'| sed 's/^git@//' |tr ':' '/'


declare -a repos_test=( \
#repos for test:
gitlab.com/arbetsformedlingen/security/vuln-report.git
gitlab.com/arbetsformedlingen/libraries/anonymisering.git 
#gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-atlas.git
#gitlab.com/arbetsformedlingen/joblinks/add-application-deadline.git
#gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-annotation-editor.git
) 

declare -a repos=( \
gitlab.com/arbetsformedlingen/security/vuln-report.git
# gitlab.com/arbetsformedlingen/devops/api-spec-to-dcat.git github token needed
gitlab.com/arbetsformedlingen/devops/cli-tools.git
gitlab.com/arbetsformedlingen/devops/jobtech-bi/data-generators/discourse-stats.git
gitlab.com/arbetsformedlingen/devops/gitlab-cicd/gitlab-tools.git
gitlab.com/arbetsformedlingen/devops/gitlab-cicd/example-for-scripts.git
gitlab.com/arbetsformedlingen/devops/hello-world.git
gitlab.com/arbetsformedlingen/devops/loaderio-token-server.git
gitlab.com/arbetsformedlingen/devops/ns-conf.git
gitlab.com/arbetsformedlingen/devops/redirecter.git
gitlab.com/arbetsformedlingen/devops/jobtech-ci-testcases/jobtech-ci-python-hello.git
gitlab.com/arbetsformedlingen/education/education-api.git
gitlab.com/arbetsformedlingen/education/education-enrich-educations.git
gitlab.com/arbetsformedlingen/education/education-frontend-app.git
gitlab.com/arbetsformedlingen/education/education-merge-educations.git
gitlab.com/arbetsformedlingen/education/education-minio-to-opensearch.git
gitlab.com/arbetsformedlingen/education/education-opensearch.git
gitlab.com/arbetsformedlingen/education/education-prune-educations.git
gitlab.com/arbetsformedlingen/education/education-scraping.git
gitlab.com/arbetsformedlingen/enrichment/jobtech-jobad-enrichments-tf-serving.git
gitlab.com/arbetsformedlingen/enrichment/jobtech-jobad-enrichments.git
gitlab.com/arbetsformedlingen/enrichment/jobtech-jobad-enrichments-frontend-v2.git
gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-backend.git
gitlab.com/arbetsformedlingen/individdata/axa-pilot/af-connect-services-frontend.git
gitlab.com/arbetsformedlingen/individdata/axa-pilot/auth-proxy.git
gitlab.com/arbetsformedlingen/individdata/axa-pilot/fake-backend.git
gitlab.com/arbetsformedlingen/individdata/axa-pilot/test-db-cleaner.git
gitlab.com/arbetsformedlingen/job-ads/development-tools/jobsearch-ad-mock-api.git
gitlab.com/arbetsformedlingen/job-ads/job-ads-env-init.git
gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis.git
gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers.git
gitlab.com/arbetsformedlingen/job-ads/historical-ads-frontend.git
gitlab.com/arbetsformedlingen/job-ads/historical-importer.git
gitlab.com/arbetsformedlingen/job-ads/search-trends.git
gitlab.com/arbetsformedlingen/job-ads/jobad-links/jobad-links-api.git
gitlab.com/arbetsformedlingen/job-ads/jobsearch/jobsearch-api.git
gitlab.com/arbetsformedlingen/job-ads/JobStream/jobstream-api.git
gitlab.com/arbetsformedlingen/joblinks/add-application-deadline.git
gitlab.com/arbetsformedlingen/joblinks/add-id.git
gitlab.com/arbetsformedlingen/joblinks/add-jobad-enrichments-api-results.git
gitlab.com/arbetsformedlingen/joblinks/add-municipality-from-place.git
gitlab.com/arbetsformedlingen/joblinks/ad-patcher.git
gitlab.com/arbetsformedlingen/joblinks/alerts.git
gitlab.com/arbetsformedlingen/joblinks/api-clients/ingenjorsjobb.se.git
gitlab.com/arbetsformedlingen/joblinks/chat-notify.git
gitlab.com/arbetsformedlingen/joblinks/common-cli-tools.git
gitlab.com/arbetsformedlingen/joblinks/date-processor.git
gitlab.com/arbetsformedlingen/joblinks/detect-language.git
gitlab.com/arbetsformedlingen/joblinks/extract-brief-description.git
gitlab.com/arbetsformedlingen/joblinks/filebeat.git
gitlab.com/arbetsformedlingen/joblinks/filter-missing-fields.git
gitlab.com/arbetsformedlingen/joblinks/import-to-elastic.git
gitlab.com/arbetsformedlingen/joblinks/job_ad_hash.git
gitlab.com/arbetsformedlingen/joblinks/joblinks-importer.git
gitlab.com/arbetsformedlingen/joblinks/launchotron.git
gitlab.com/arbetsformedlingen/joblinks/make-report.git
gitlab.com/arbetsformedlingen/joblinks/pipeline-list-used-images.git
gitlab.com/arbetsformedlingen/joblinks/pipeline-weekly-report.git
gitlab.com/arbetsformedlingen/joblinks/rclone.git
gitlab.com/arbetsformedlingen/joblinks/rclone-wrapper.git
gitlab.com/arbetsformedlingen/joblinks/remove-description.git
gitlab.com/arbetsformedlingen/joblinks/remove-union-representatives-from-description.git
gitlab.com/arbetsformedlingen/joblinks/scraping.git
gitlab.com/arbetsformedlingen/joblinks/sourcelinks-processor.git
gitlab.com/arbetsformedlingen/joblinks/text-2-ssyk.git
gitlab.com/arbetsformedlingen/maintained-packages/perl-lib-pkgbuilder.git
gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl.git
gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl.git
gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl.git
gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/wikipoc-nginx.git
gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api.git
gitlab.com/arbetsformedlingen/taxonomy-dev/backend/legacy-taxonomy-soap-service-java.git
gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/mentor-api.git
gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/semantic-concept-search.git
gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/text2ssyk-bert.git
gitlab.com/arbetsformedlingen/taxonomy-dev/backend/openshift-varnish.git
gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-annotation-editor.git
gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-atlas-landingpage.git
gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-atlas.git
gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-editor.git
gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-highscore.git
gitlab.com/arbetsformedlingen/taxonomy-dev/mdbook-builder.git
gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-fetch-minio.git
gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-frontend.git
gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-prepare-static-web.git
gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-tax2skos.git
gitlab.com/arbetsformedlingen/www/alljobadswidget.git
gitlab.com/arbetsformedlingen/www/jobtechdev-se.git
gitlab.com/arbetsformedlingen/www/nystartsjobb-test.git
gitlab.com/arbetsformedlingen/www/arbetsmarknadsutbildningar-susa.git
gitlab.com/arbetsformedlingen/www/arbetsgivare-test.git
gitlab.com/arbetsformedlingen/wikipoc-gemensam-k-lla/frontend/wikipoc.git
gitlab.com/open-data-knowledge-sharing/open-data-knowledge-sharing.gitlab.io.git
)

declare -a alias=( \
base
BASE
server
build
deps
scratch
runtime
)

# --severity-threshold=<low|medium|high|critical>
sever="--severity-threshold=high"

#  --app-vulns   Allow detection of vulnerabilities in your application dependencies from container images as well as from the operating system, all in one single scan.

# clear # writes ^[[2J^[[H^[[3J to the output

: '
# Part 1: test images from Nexus
# --show-vulnerable-<paths=false|none|some|all>
show="--show-vulnerable-paths=none"
imageslen=${#images[@]} # lenght of array
# loop through the above array of images in Nexus

for i in "${images[@]}"
do
   snyk container test $sever $show "$i" 
   echo "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
done
echo "Total Nexus images scanned: $imageslen" 
'

# Part 2: get images from Dockerfiles

function snyk_run() {
   declare -i total=0 exit_code=0 # declare integer var
   repo=$2 # git link to repo
   # repo="$(echo -n "$repo" | sed -E 's/([^[:alnum:]])/\\\1/g')" # add \ before special char
   # echo "Looking Docker in master or main branch: $1" 
   if [ "$(curl --silent $1 | grep -w FROM)" == "" ]
   then 
     # echo "Docker/FROM not found" 
     return 0
   else 
     # remove FROM (cut) and remove "as ","AS "
     images=$(curl --silent $1 | grep -w ^FROM | cut -c 6- | sed 's/as .*//' | sed 's/AS .*//')
     total=$(echo "$images" | wc -l)
     # echo "total images in Dockerfile: $total"
     n=${total}
     for j in $(seq 1 ${n}) 
     do 
       image=$(echo "${images}" | sed "${j}q;d" | tr -d '[:space:]') # take j:n word of image list, trim spaces
       # if [[ $image =~ "base" || $image =~ "BASE" || $image =~ "server" || $image =~ "build" || $image =~"deps" ]]
       if [[ " ${alias[*]}  " =~ " ${image} " ]]
       then total=$((total-1)); continue # total-1 images when "FROM <alias>", next for
       fi
       # echo "Docker image to parse to Snyk: ${image}" 
       # JSON="--json" for json output cronjob run
       if [[ $JSON == "--json" ]] # add repo key pair inside the json blob with jq
       then
         snyk=$(snyk container test $sever --fail-on=upgradable --username="afiris" --password="$DOCKER_PWD" "$JSON" ${image})
         exit_code=$?
         echo $snyk | jq --arg repo "$repo" '.repo=$repo' #add repo key-value
         echo "," # add , between json blobs
       else
         snyk container test $sever --fail-on=upgradable --username="afiris" --password="$DOCKER_PWD" "$JSON" ${image}
         exit_code=$?
       fi 
       
       if (( $exit_code == 1 )) # snyk returns 1 when found vulnerability
       then   
         vuln=$((vuln+1)) 
       elif (( $exit_code == 2 )) # sometimes snyk output is {"type":"Buffer","data":[]} or connect ECONNREFUSED 127.0.0.1:443 then exit code=2
       then 
         prefix="docker.io/"
         image=${image//$prefix} # remove docker.io/ 
         # echo "try parse image name without 'docker.io/': $image"
         if [[ $JSON == "--json" ]] # add repo key pair inside the json blob with jq
         then
           snyk=$(snyk container test $sever --fail-on=upgradable --username="afiris" --password="$DOCKER_PWD" "$JSON" ${image})
           exit_code=$?
           echo $snyk | jq --arg repo "$repo" '.repo=$repo' #add repo key-value
           echo "," # add , between json blobs
         else
           snyk container test $sever --fail-on=upgradable --username="afiris" --password="$DOCKER_PWD" "$JSON" ${image}
           exit_code=$?
         fi
         if (( $exit_code == 1 ))
         then
           vuln=$((vuln+1))
         fi
       elif (( $exit_code != 0 )) # if not 0,1,2
       then
         echo "not usual snyk exit code: $exit_code"
       fi
     done

     return $total
   fi
}

if [[ $JSON == "--json" ]] # starting [ for json
then 
     echo "["
fi

declare -i num_var=0 exit=0 total_vuln=0 vuln=0
# git="https://$git/-/raw/master/Dockerfile"
# echo "git:$git"
suffix=".git"
for i in "${repos[@]}"
do
   git=${i%"$suffix"} # remove .git from the end
   docker="https://$git/-/raw/main/Dockerfile" # try main branch
   # echo "docker raw: $docker"
   # if curl --head --silent -o --fail $docker 2> /dev/null # check if Dockerfile exists, but 404 gives ok resultat!
   snyk_run ${docker} ${git}
   exit=$?
   num_var=$num_var+$exit #add return value from funk snyk_run
   if [[ $exit == 0 ]] # if return of func equals 0 aka FROM not found in main
   then
     docker="https://$git/-/raw/master/Dockerfile" # try master branch
     # echo "docker raw: $docker"
     snyk_run ${docker} ${git}
     exit=$?
     num_var=$num_var+$exit
   fi
   total_vuln=$total_vuln+$vuln
   vuln=0
   if [[ $JSON != "--json" ]] # escape print in json output
   then 
     echo "REPO checked: ${git}"
     echo "runtime scanned images: $num_var and vulnerable: $total_vuln <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
   fi
done

if [[ $JSON == "--json" ]] # closing void {} and ] for json
  then 
     echo "{\"totalImagesScanned\":\"$num_var\","
     echo "\"totalVulnerable\":\"$total_vuln\"}]"
  else
     echo "Total images from all Dockerfiles scanned: $num_var Total vulnerable: $total_vuln."
fi


