#!/usr/bin/env bash
set -eEu -o pipefail # check bash strict mode

msg="$1"
file="$2"
server="${3:-https://mattermost.jobtechdev.se}" # :- <default value>

if [ ! -f "$file" ]; then echo "**** cannot find file $file" >&2; exit 1; fi


## send message with attached file
trap "echo '**** failed to upload file' >&2" ERR
file_id=$(mattermost "https://mattermost.jobtechdev.se" "$CHANID" "$file" "text/plain" "$BOTAUTH" | tee /tmp/response | jq -r .file_infos[].id)
echo "fileid: ${file_id}"

echo "upload response:" >&2
echo "============================" >&2
cat /tmp/response >&2
echo "============================" >&2
echo >&2


curl -k "$server"'/api/v4/posts' \
-H 'Authorization: Bearer '"$BOTAUTH" \
-H 'Content-Type: text/plain' \
--data-binary '{"channel_id": "'"$CHANID"'", "message": "'"$msg"'", "file_ids": ["'"$file_id"'"]}' 
