FROM docker.io/library/ubuntu:24.04 as build

ADD mattermost.cpp /app/

RUN apt-get -y update &&\
    apt-get -y --no-install-recommends install ca-certificates curl g++ libcurl4-openssl-dev wget &&\
    c++ -o /app/mattermost /app/mattermost.cpp -l curl &&\
    wget -O /usr/local/bin/mc https://dl.min.io/client/mc/release/linux-amd64/archive/mc.RELEASE.2024-11-21T17-21-54Z &&\
    chmod +x /usr/local/bin/mc &&\
    curl -k https://static.snyk.io/cli/v1.1295.3/snyk-linux -o snyk &&\
    chmod +x ./snyk &&\
    mv ./snyk /usr/local/bin/

FROM docker.io/library/ubuntu:24.04 as runtime

ADD snyk.sh upload_mattermost.sh run.sh /app/

COPY --from=build /app/mattermost /app/mattermost
COPY --from=build /usr/local/bin/mc /usr/local/bin/mc
COPY --from=build /usr/local/bin/snyk /usr/local/bin/snyk 

RUN apt-get -y update &&\
    apt-get -y --no-install-recommends install build-essential ca-certificates curl g++ gnuplot jq libcurl4-openssl-dev &&\
    chmod a+rx /app/*.sh &&\
    apt-get -y remove perl && apt-get -y autoremove --purge && apt-get -y clean

WORKDIR /app

# for to run mattermost c++ in upload_mattermost.sh
ENV PATH="/app:${PATH}"

CMD run.sh
