#!/usr/bin/env bash
set -eEu -o pipefail # check bash strict mode

DATE="$(date +%V)"

snyk config set ORG=Jobtech
snyk config set disable-analytics=true
echo "snykauth: $SNYKAUTH"
snyk -d auth $SNYKAUTH

if [ $JSON == "--json" ]
then 
  echo "using json output (no snyk output to stdout)!"
  SNYK_OUTPUT=/tmp/snyk_"$DATE".json
  # bash snyk.sh > "$SNYK_OUTPUT"
  bash snyk.sh | tee "$SNYK_OUTPUT" # for debug

  SNYK_SUM=/tmp/snyk_summary_"$DATE".json
  cat $SNYK_OUTPUT | jq -r '.[] | {baseImage:.docker.baseImage, repo:.repo, baseImageRemediation_Advice_message:.docker.baseImageRemediation.advice[0].message | tostring | select(contains("critical"))}' >  "$SNYK_SUM"
  cat $SNYK_OUTPUT | jq -r '.[-1]' >> "$SNYK_SUM" # add last element of json array with total info
  bash upload_mattermost.sh "JSON Summary: $DATE" "$SNYK_SUM"
else
  SNYK_OUTPUT=/tmp/snyk_"$DATE".txt
  bash snyk.sh | tee "$SNYK_OUTPUT" # tee (not >): to see stdout in openshift
fi

bash upload_mattermost.sh "Weekly Vulnerability Report Week: $DATE" "$SNYK_OUTPUT"

mc alias set vuln https://minio.arbetsformedlingen.se vuln $MINIO_PWD # set alias in minio client with MINIO_PWS from secrets repo
mc cp $SNYK_OUTPUT vuln/vuln/ #copy file to minio server

if [ $JSON == "--json" ]
then
  mc cp $SNYK_SUM vuln/vuln/ #copy summary json file to minio server 
fi
